NAME=collatz
all: build clean

build:
	ghc -Wall $(NAME).hs -o $(NAME)
clean:
	rm $(NAME).o
	rm $(NAME).hi
