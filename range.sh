#!/bin/bash

if [ "$#" != "1" ] 
then
    echo "Incorrect amout of arguments"
    exit 1
fi

for i in $(seq 2 $1)
do
    printf "$i " && ./collatz $i 2>/dev/null  
done
