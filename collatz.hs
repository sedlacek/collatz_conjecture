import Debug.Trace
import System.Environment

collatz :: Integer -> String
collatz x 
    | x == 1 = trace (show x) "-> 1"
    | even(x) = trace (show x) collatz $ div x 2
    | otherwise = trace (show x) collatz $ (x * 3 + 1) `div` 2

main :: IO()
main = do
    args <- getArgs
    putStr $ collatz $ read $ args!!0
    putStr "\n"
